<?php
$db = ffDB_Sql::factory();
$db->query("SELECT * FROM `cm_gs`");
if (!$db->numRows())
{
	mod_gs_backend_populate();
}

$obj = ffGrid::factory($cm->oPage);
$obj->id = "cm-gs";
$obj->title = "Campagne GetResponse";
$obj->resources[] = "cm_gs";
$obj->source_SQL = "SELECT
								`cm_gs`.*
							FROM
								`cm_gs`
							[WHERE]
							[HAVING]
							[ORDER]
		";
$obj->record_id = "cm-gs-modify";
$obj->record_url = FF_SITE_PATH . $cm->path_info . "/modify";
$obj->order_default = "ID";
$obj->display_delete_bt = false;
$obj->display_new = false;
$obj->display_edit_url = false;

$field = ffField::factory($cm->oPage);
$field->id = "ID";
$obj->addKeyField($field);

$field = ffField::factory($cm->oPage);
$field->id = "ID";
$field->label = "API KEY";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "optin";
$field->label = "Tipo Optin";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "is_default";
$field->base_type = "Number";
$field->label = "Default";
$field->extended_type = "Selection";
$field->multi_pairs = array(
	array(new ffData(1, "Number"), new ffData("Si"))
);
$field->multi_select_one_label = "";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "from_email";
$field->label = "E-Mail FROM";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "name";
$field->label = "Nome";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "reply_to_email";
$field->label = "E-Mail reply-to";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "created_on";
$field->base_type = "DateTime";
$field->label = "Creata il";
$obj->addContent($field);

$field = ffField::factory($cm->oPage);
$field->id = "from_name";
$field->label = "Nome FROM";
$obj->addContent($field);

$cm->oPage->addContent($obj);

function mod_gs_backend_populate()
{
	$db = ffDB_Sql::factory();
	$api = new GetResponse(GETRESPONSE_API);
	$campaigns 	 = (array)$api->getCampaigns();
	foreach ($campaigns as $key => $value)
	{
		$campaign_id	= new ffData($key);
		$optin			= new ffData($value->optin);
		$is_default		= new ffData(($value->is_default === "yes" ? 1 : 0), "Number");
		$from_email		= new ffData($value->from_email);
		$name			= new ffData($value->name);
		$reply_to_email = new ffData($value->reply_to_email);
		$created_on		= new ffData($value->created_on, "DateTime", "ISO9075");
		$from_name		= new ffData($value->from_name);
		
		$sSQL = "UPDATE `cm_gs` SET
						`optin` = " . $db->toSql($optin) . "
						, `is_default` = " . $db->toSql($is_default) . "
						, `from_email` = " . $db->toSql($from_email) . "
						, `name` = " . $db->toSql($name) . "
						, `reply_to_email` = " . $db->toSql($reply_to_email) . "
						, `created_on` = " . $db->toSql($created_on) . "
						, `from_name` = " . $db->toSql($from_name) . "
					WHERE
						`ID` = " . $db->toSql($campaign_id) . "
			";
		$db->execute($sSQL);
		if (!$db->affectedRows())
		{
			$sSQL = "INSERT INTO `cm_gs` (
							`ID`
							, `optin`
							, `is_default`
							, `from_email`
							, `name`
							, `reply_to_email`
							, `created_on`
							, `from_name`
						) VALUES (
							" . $db->toSql($campaign_id) . "
							, " . $db->toSql($optin) . "
							, " . $db->toSql($is_default) . "
							, " . $db->toSql($from_email) . "
							, " . $db->toSql($name) . "
							, " . $db->toSql($reply_to_email) . "
							, " . $db->toSql($created_on) . "
							, " . $db->toSql($from_name) . "
						)";
			$db->execute($sSQL);
		}
	}
}
