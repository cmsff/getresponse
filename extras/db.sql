--
-- Table structure for table `cm_gs`
--

CREATE TABLE IF NOT EXISTS `cm_gs` (
  `ID` varchar(255) NOT NULL,
  `optin` varchar(255) NOT NULL,
  `is_default` int(1) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reply_to_email` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `from_name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

