<?php
$globals = ffGlobals::getInstance("modules_getresponse");
$globals->applet_params = $applet_params;
$cm->loaded_applets[$appletid]["comps"] = array("gsSubscribe");

$filename = cm_cascadeFindTemplate("/applets/subscribe/default.html", "getresponse");
/*
$filename = cm_moduleCascadeFindTemplate(FF_THEME_DISK_PATH, "/modules/getresponse/applets/subscribe/default.html", $cm->oPage->getTheme(), false);
if ($filename === null)
	$filename = cm_moduleCascadeFindTemplate(CM_MODULES_ROOT . "/getresponse/themes", "/applets/subscribe/default.html", $cm->oPage->getTheme());
*/
$tpl = ffTemplate::factory(ffCommon_dirname($filename));
$tpl->load_file(basename($filename), "main");

$obj = ffRecord::factory($cm->oPage);
$obj->id = "gsSubscribe";
$obj->tpl[0] = $tpl;
$obj->skip_action = true;
$obj->addEvent("on_done_action", "my_on_done_action");
$obj->use_own_location = true;
$cm->applets_components[$appletid]["gsSubscribe"] = true;
$obj->buttons_options["cancel"]["display"] = false;
$obj->buttons_options["insert"]["label"] = "Iscriviti";
$obj->buttons_options["insert"]["jsaction"] = "javascript:ff.ajax.doRequest({
		'component' : 'gsSubscribe', 
		'action' : 'gsSubscribe_insert', 
		'fields' : ff.getFields(jQuery('#gsSubscribe')) 
	});";

$fld_email = ffField::factory($cm->oPage);
$fld_email->id = "email";
$fld_email->label = "E-Mail";
$fld_email->required = true;
$fld_email->addValidator("email");
$obj->addContent($fld_email);

$fld_name = ffField::factory($cm->oPage);
$fld_name->id = "fullname";
$fld_name->label = "Nome";
$obj->addContent($fld_name);

if (GETRESPONSE_APPLET_SUBSCRIBE_FIELD_PRIVACY && (
		!ffIsset($globals->applet_params, "field_privacy")
		|| $globals->applet_params["field_privacy"]
	))
{
	$fld_priv = ffField::factory($cm->oPage);
	$fld_priv->id = "privacy";
	$fld_priv->label = "Privacy";
	$fld_priv->base_type = "Number";
	$fld_priv->extended_type = "Boolean";
	$fld_priv->unchecked_value = new ffData(0, "Number");
	$fld_priv->checked_value = new ffData(1, "Number");
	$fld_priv->required = true;
	//$fld_priv->fixed_post_content = "<a href=\"#\" onclick=\"javascript:ff.ffPage.dialog.doOpen('tos', '" . FF_SITE_PATH . "/tos')\">Noi rispettiamo la tua privacy</a>";
	$obj->addContent($fld_priv);
}

$cm->oPage->addContent($obj);

function my_on_done_action(ffRecord_html $obj, $action)
{
	$cm = cm::getInstance();
	
	switch ($action)
	{
		case "insert":
			$globals = ffGlobals::getInstance("modules_getresponse");
			
			$email = $obj->form_fields["email"]->value->getValue();
			$fullname = $obj->form_fields["fullname"]->value->getValue();
			if (!strlen($fullname))
				$fullname = $email;
			
			$campaign_id = ($globals->applet_params["CAMPAIGN_ID"] ? $globals->applet_params["CAMPAIGN_ID"] : GETRESPONSE_CAMPAIGN_ID);
			$campaign_customs = ($globals->applet_params["CUSTOMS"] ? $globals->applet_params["CUSTOMS"] : GETRESPONSE_CUSTOMS);
			
			if (is_string($campaign_customs))
			{
				$tmp = array();
				$tmp_pairs = explode(";", $campaign_customs);
				foreach ($tmp_pairs as $value)
				{
					$tmp_sub = explode(">", $value);
					$tmp[$tmp_sub[0]] = $tmp_sub[1];
				}
				$campaign_customs = $tmp;
			}
			
			$res = cm::getInstance()->modules["getresponse"]["events"]->doEvent("on_before_add", array($campaign_id, $fullname, $email, $campaign_customs));
			$rc = end($res);
			if ($rc !== null)
			{
				if (is_array($rc))
				{
					if (ffIsset($rc, "campaign_id"))
						$campaign_id = $rc["campaign_id"];
					if (ffIsset($rc, "fullname"))
						$fullname = $rc["fullname"];
					if (ffIsset($rc, "email"))
						$email = $rc["email"];
					if (ffIsset($rc, "campaign_customs"))
						$campaign_customs = $rc["campaign_customs"];
				}
			}
			
			$gsApi = new GetResponse(GETRESPONSE_API);
			$ret = $gsApi->addContact(
						$campaign_id
						, $fullname
						, $email
						, 'standard'
						, 0
						, $campaign_customs
					);
			
			if ($ret === null)
			{
				$obj->strError = "Errore nell'iscrizione, si prega di riprovare in seguito";
			}
			else if (is_object($ret))
			{
				if ($ret->queued)
					$success = true;
				elseif ($ret->code)
				{
					switch ($ret->code)
					{
						case -1:		$obj->strError = "L'email indicata è già stata aggiunta alla campagna"; break;
						case -32602:	$obj->strError = "Parametri non validi"; break;
						default:		$obj->strError = "Errore non gestito: " . $obj->message; break;
					}
				}
			}
			else
			{
				var_dump($ret);
				exit;
			}
			
			if (/*false && */!$success)
			{
				//var_dump($ret); exit;
				$obj->contain_error = true;
			}
			else
			{
                $filename = cm_cascadeFindTemplate("/applets/subscribe/subscribed.html", "getresponse");
                /*
				$filename = cm_moduleCascadeFindTemplate(FF_THEME_DISK_PATH, "/modules/getresponse/applets/subscribe/subscribed.html", $cm->oPage->getTheme(), false);
				if ($filename === null)
					$filename = cm_moduleCascadeFindTemplate(CM_MODULES_ROOT . "/getresponse/themes", "/applets/subscribe/subscribed.html", $cm->oPage->getTheme());
				*/
				$obj->tpl[0]->template_root = ffCommon_dirname($filename);
				$obj->tpl[0]->load_file(basename($filename), "main");
			}
			return true;
	}
}
